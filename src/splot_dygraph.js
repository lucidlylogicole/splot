// splot_dygraph.js 1.1.0    (c) 2024 lucidlylogicole    Released under the MIT License

function Splot(options) {
    // Load splot options
    //---o:
    let splot_options = {
        parent:undefined,   // parent element (id or element)
        type:'scatter',     // scatter, bar, line, pie, 
        title:'',           // chart title
        xlabel:'',          // x axis label
        ylabel:'',          // y axis label
        xzero:false,        // x axis start at 0
        yzero:false,        // y axis start at 0
        legend:false,       // display legend
        labels:undefined,   // array of labels for non scatter type charts
        // dark:false,         // dark colors mode
        grid:true,          // show grid
        
        y2:false,           // show 2nd y axis
        y2zero:false,       // y2 axis start at 0
        y2label:'',
        dygraph:{},         // override dygraph options
    }
    for (let key in options) {
        splot_options[key] = options[key]
    }
    
    let chart_options = {
        title:splot_options.title,
        xlabel:splot_options.xlabel,
        ylabel:splot_options.ylabel,
        gridLineColor:'rgba(100,100,100,0.5)',
        colors:[],
        labelsSeparateLines:true,
    }
    
    if (splot_options.legend) {
        chart_options.legend = 'always'
    } else {
        chart_options.legend = 'never'
    }
    
    if (!splot_options.grid) {
        chart_options.drawGrid = false
    }
    
    if (typeof(splot_options.parent) == 'string') {
        splot_options.parent = document.getElementById(splot_options.parent)
    }

    //---Merge Options
    function mergeObject(obj_src, obj_upd) {
        for (let key in obj_upd) {
            if (!(key in obj_src)){
                obj_src[key] = obj_upd[key]
            } else {
                // Keep walking
                let val = obj_upd[key]
                if (typeof(val) == 'object' && !(Array.isArray(val))) {
                    mergeObject(obj_src[key],obj_upd[key])
                } else {
                    obj_src[key] = obj_upd[key]
                }
            }
        }
    }
    mergeObject(chart_options,splot_options.dygraph)

    let chart = new Dygraph(splot_options.parent,"X\n",chart_options)
    
//---o:
    let Chart = {
        x:[],
        series:[],
        data:[],
        xlabel:'',
    }
    
    function getX() {
        // Combine datasets to get 1 x axis
        let x = []
        let cnt = 0
        for (let si of Chart.series) {
            let xi = si.x
            cnt++
            let i = 0
            for (let ii=0; ii <xi.length; ii++) {
                if (x.length <= ii || x.length <= i) {
                    x.push(xi[ii])
                    i+=1
                } else {
                    if (x[i] == xi[ii]) {
                        // skip
                        i++
                    } else if (x[i] > xi[ii]) {
                        // prepend
                        x.splice(i,0,xi[ii])
                        i++
                    } else if (xi[ii] > x[i]) {
                        // wait and see if next 
                        i++
                        ii--
                    }
                }
            }
        }
        Chart.x = x
        return x
    }

    const COLORS = [
        'rgba(77,201,246,0.8)', // Blue
        'rgba(252,51,51,0.8)',  // Red
        'rgba(50,208,16,0.8)',  // Green
        'rgba(133,73,186,0.8)', // Purple
        'rgba(255,105,28,0.8)', // Orange
        'rgba(245,55,148,0.8)', // Pink
        'rgba(143,86,0,0.8)',   // Brown
        'rgba(0,102,183,0.8)',  // Dark Blue
        'rgba(97,129,1,0.8)',   // Dark Green Yellow
        'rgba(143,7,0,0.8)',    // Dark Red
    ]

//---Plot Object
    return {
        chart:chart,
        _color_count:0,
        
        getColor: function(index) {
            if (index === undefined) {index = this._color_count % COLORS.length}
            let clr = COLORS[index]
            this._color_count++
            return clr
        },
        
        clear:function() {
            Chart.x = []
            Chart.series = []
            Chart.data = []
            this._color_count=0
            updateOptions({file:"X\n"})
        },
        
        addData:function(dataset) {
            let data_options = {
                drawPoints:true,
                pointSize:3,
                strokeWidth:2,
                connectSeparatedPoints:true,  // needed for multiple datasets
            }
            
            for (let key in data_options) {
                if (key in dataset) {
                    data_options[key] = dataset[key]
                }
            }
            
            // Point Style
            if (dataset.points == 0) {
                data_options.drawPoints=false
            } else if (dataset.points !== undefined) {
                data_options.pointSize = dataset.points
            }
            // Line Style
            if (dataset.line !== undefined){data_options.strokeWidth = dataset.line}
            if ('lineDash' in dataset) {data_options.strokePattern = dataset.lineDash}
            
            // Automatically add label if left off
            if (!('label' in dataset)) {dataset.label = 'data '+(Chart.series.length+1)}
            
            // Handle if data array is specified instead of x and y
            if (dataset.data !== undefined) {
                dataset.x = []
                dataset.y = []
                // Check array length and handle 1D
                let arr_len = dataset.data[0].length
                if (arr_len === undefined) {
                    // 1D data array
                    dataset.y = dataset.data
                    for (let i=0; i<dataset.data.length; i++) {
                        dataset.x.push(i)
                    }
                    
                } else {
                    // 2D data array
                    for (let i=0; i<dataset.data.length; i++) {
                        if (arr_len == 1) {
                            dataset.x.push(i)
                            dataset.y.push(dataset.data[i][0])
                            
                        } else {
                            dataset.x.push(dataset.data[i][0])
                            dataset.y.push(dataset.data[i][1])
                        }
                    }
                }
                
            }
            
            // Setup color
            let color = dataset.color
            if (color === undefined){color = this.getColor()}
            chart_options.colors.push(color)
            
            Chart.series.push({x:dataset.x,y:dataset.y,label:dataset.label,options:data_options})
            if (dataset.update === undefined || dataset.update) {
                this.update()
            }
        },
        
        update: function() {
            // Get X
            let x = getX()
            let sD = {}
            // sD[chart_options.xlabel]={}
            // Get Labels
            let lbls = [chart_options.xlabel]
            for (let si of Chart.series) {
                lbls.push(si.label)
                sD[si.label]=si.options
            }
            
            // Get Ys
            let data = []
            for (let i=0; i < x.length; i++) {
                let rw = [x[i]]
                for (let si of Chart.series) {
                    let ind = si.x.indexOf(x[i])
                    if (ind > -1) {
                        rw.push(si.y[ind])
                    } else {
                        rw.push(null)
                    }
                }
                data.push(rw)
            }
            
            Chart.data = data
            chart.updateOptions({'file':data, 'labels':lbls, series:sD})
            
            // Styling
            splot_options.parent.querySelector('.dygraph-xlabel').style['text-align']="center"
            // splot_options.parent.querySelector('.dygraph-ylabel').style['text-align']="right"
            splot_options.parent.querySelector('.dygraph-title').style['text-align']="center"
            splot_options.parent.querySelector('.dygraph-legend').style['text-align']="right"
            
        },
        
        resetZoom: function() {
            chart.resetZoom()
        },
        
    }
    
    
    
}