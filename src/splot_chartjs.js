// splot_chartjs.js 1.1.0    (c) 2024 lucidlylogicole    Released under the MIT License

function Splot(options) {
    // Load splot options
    //---o:
    let splot_options = {
        parent:undefined,   // parent element (id or element)
        type:'scatter',     // scatter, bar, line, pie, 
        title:'',           // chart title
        xlabel:'',          // x axis label
        ylabel:'',          // y axis label
        xzero:false,        // x axis start at 0
        yzero:false,        // y axis start at 0
        legend:false,       // display legend
        labels:undefined,   // array of labels for non scatter type charts
        grid:true,          // display gridlines
        dark:false,         // dark colors mode
        
        y2:false,           // show 2nd y axis
        y2zero:false,       // y2 axis start at 0
        y2label:'',         // y2 axis label
        
        chartjs:{},         // override chartjs options
        
        decimation:false,   // Enable chartjs decimation
    }
    for (let key in options) {
        splot_options[key] = options[key]
    }

    //---o:
    let chart_options = {
        type: splot_options.type,
        //---o:
        data: {
            datasets: [],
            labels:splot_options.labels,
        },
        //---o:
        options: {
            // color:'white',
            responsive: true,
            maintainAspectRatio:false,
            resizeDelay:0.5,
            elements:{
                point:{
                    borderWidth:0,
                },
                line: {
                    borderWidth:1,
                },
            },
            scales: {
                x: {
                    beginAtZero: splot_options.xzero,
                    title:{
                        text:splot_options.xlabel,
                        display:splot_options.xlabel!= '',
                    },
                },
                y: {
                    beginAtZero: splot_options.yzero,
                    title:{
                        text:splot_options.ylabel,
                        display:splot_options.ylabel!= '',
                    },
                },
                y2:{
                    beginAtZero: splot_options.y2zero,
                    display:splot_options.y2,
                    position: 'right',
                    title:{
                        text:splot_options.y2label,
                        display:splot_options.y2label!= '',
                    },
                },
            },
            
            animation: {duration: 0}, // Disable Animation
            
            //---o:
			plugins: {
			    title:{
			        text:splot_options.title,
			        display:splot_options.title != '',
			    },
                //---o:
                legend:{
                    position:'bottom',
                    reverse:true,
                    display:splot_options.legend,
                },
                //---o:
				zoom: {
					pan: {
						enabled: true,
						mode: 'xy',
						scaleMode: 'y',
					    modifierKey:'ctrl',
					},
					zoom: {
				// 		wheel: {
				// 			enabled: true,
				// 		},
						pinch: {
							enabled: true,
						},
						drag:{
						    enabled:true,
						    backgroundColor:'rgba(247,228,23,0.2)',
						},
						mode: 'xy',
					}
				}
			}
        },
    }
    
    //---Decimation
    if (splot_options.decimation) {
        chart_options.options.plugins.decimation = {enabled:true, algorithm:'min-max'}
        chart_options.type = 'line'
    }
    
    //---Dark Mode
    if (splot_options.dark) {
        for (let axis of ['x','y','y2']) {
            chart_options.options.scales[axis].title.color='#ccc'
            chart_options.options.scales[axis].grid={color:'#444'}
            chart_options.options.scales[axis].ticks={color:'#ccc'}
        }
        chart_options.options.color='#ccc'  // default
        chart_options.options.plugins.title.color='#eee'
    }
    
    // Grid
    if (!splot_options.grid) {
        for (let axis of ['x','y','y2']) {
            chart_options.options.scales[axis].grid = {display:false}
        }
    }
    
    //---Merge Chartjs Options
    function mergeObject(obj_src, obj_upd) {
        for (let key in obj_upd) {
            if (!(key in obj_src)){
                obj_src[key] = obj_upd[key]
            } else {
                // Keep walking
                let val = obj_upd[key]
                if (typeof(val) == 'object' && !(Array.isArray(val))) {
                    mergeObject(obj_src[key],obj_upd[key])
                } else {
                    obj_src[key] = obj_upd[key]
                }
            }
        }
    }
    mergeObject(chart_options,splot_options.chartjs)
    
    //---Create Chart
    if (typeof(splot_options.parent) == 'string') {
        splot_options.parent = document.getElementById(splot_options.parent)
    }
    let cnv = document.createElement('canvas')
    
    splot_options.parent.appendChild(cnv)
    let chart = new Chart(cnv,chart_options)
    cnv.addEventListener('dblclick', function(){chart.resetZoom()},false)
    
    //---o:
    const COLORS = [
        'rgba(77,201,246,0.8)', // Blue
        'rgba(252,51,51,0.8)',  // Red
        'rgba(50,208,16,0.8)',  // Green
        'rgba(133,73,186,0.8)', // Purple
        'rgba(255,105,28,0.8)', // Orange
        'rgba(245,55,148,0.8)', // Pink
        'rgba(143,86,0,0.8)',   // Brown
        'rgba(0,102,183,0.8)',  // Dark Blue
        'rgba(97,129,1,0.8)',   // Dark Green Yellow
        'rgba(143,7,0,0.8)',    // Dark Red
    ]
    
    //---o:
    const MARKERS = [
        'circle',
        'rect',
        'triangle',
        'cross',
        'star',
        'rectRot',
        'crossRot',
        'rectRounded',
        // 'line',
        // 'dash',
    ]
    
//---Plot Object
    return {
        chart:chart,
        _color_count:0,
        
        clear: function() {
            // Clear All Data
            this._color_count = 0
            chart.data.labels.pop();
            chart.data.datasets=[]
            chart.update()
            chart.resetZoom()
        },
        
        getColor: function(index) {
            if (index === undefined) {index = this._color_count % COLORS.length}
            let clr = COLORS[index]
            this._color_count++
            return clr
        },
        
        getMarker: function(index) {
            if (index === undefined) {index = this._color_count % MARKERS.length}
            return MARKERS[index]
        },
        
        setTitles: function(titles) {
            if (titles.title !== undefined) {
                chart.options.plugins.title.text = titles.title
                chart.options.plugins.title.display = titles.title != ''
            }
            if (titles.xlabel !== undefined) {
                chart.options.scales.x.title.text = titles.xlabel
                chart.options.scales.x.title.display = titles.xlabel != ''
            }
            if (titles.ylabel !== undefined) {
                chart.options.scales.y.title.text = titles.ylabel
                chart.options.scales.y.title.display = titles.ylabel != ''
            }
            if (titles.y2label !== undefined) {
                chart.options.scales.y2.title.text = titles.y2label
                chart.options.scales.y2.title.display = titles.y2label != ''
            }
            chart.update()
        },
        
        addData: function(dataset) {
            // Default Options
            let data_options = {
                type:'scatter',
                label: '',
                showLine:true,
                pointRadius:3,
                labels:undefined,
                hidden:false,
                update:true,
                order:-chart.data.datasets.length, // last added plot on top
                data:[],
            }
            // Setup default options
            for (let key in data_options) {
                if (key in dataset) {
                    data_options[key] = dataset[key]
                }
            }
            
            // parse data if x,y defined
            if (data_options.data.length==0) {
                if (data_options.type == 'bar' || data_options.type == 'line') {
                    data_options.data = dataset.x
                } else if (data_options.type == 'scatter' && data_options.data.length==0) {
                    let data = []
                    for (let i=0; i<dataset.x.length; i++) {
                        data.push({x:dataset.x[i],y:dataset.y[i]})
                    }
                    data_options.data = data
                }
            }
            
            // Get Line and Points Sizes
            if ('line' in dataset) {data_options.showLine = dataset.line > 0;data_options.borderWidth=dataset.line}
            if ('points' in dataset) {data_options.pointRadius = dataset.points}
            if ('lineDash' in dataset) {data_options.borderDash = dataset.lineDash}
            if ('yaxis' in dataset) {data_options.yAxisID = dataset.yaxis}
            
            // get color
            if ('color' in dataset) {
                data_options.borderColor = dataset.color
                data_options.backgroundColor = dataset.color
            } else {
                let clr = this.getColor()
                data_options.borderColor = clr
                data_options.backgroundColor = clr
            }
            
            // get point style
            if ('marker' in dataset) {
                data_options.pointStyle = dataset.marker
            } else if (!('color' in dataset) && data_options.pointRadius) {
                // Auto load style
                style_index = Math.round(this._color_count/COLORS.length)
                data_options.pointStyle=this.getMarker(style_index)
            }
            
            chart.data.datasets.push(data_options)
            if (data_options.update) {
                chart.update()
                chart.resetZoom()
            }
        },
        
        data: function(index) {
            return chart.data.datasets[index]
        },
        
        removeData: function(index, update) {
            chart.data.datasets.splice(index,1)
            if (update || update===undefined) {
                chart.update()
            }
        },
        
        update: function() {
            chart.update()
        },
        
        resetZoom: function() {
            chart.resetZoom()
        },
    }
}