# Splot
A simple JavaScript plotting library that provides a quicker way to create charts. The Splot API works with multiple JavaScript chart libraries, including [Chart.js](https://www.chartjs.org/) and [Dygraphs](https://dygraphs.com/).

## Features
- default chart type is scatter plot with lines
- later plots show up on top of first plots (by default)
- automatic color and marker selection if not specified
- zoom enabled by default
- similar api for both chartjs and dygraphjs
- can still specify most Chartjs and Dygraphs options

## Downloads
- [splot_chartjs.min.js](dist/splot_chartjs.min.js) - the Chartjs implementation of Splot, which includes the minified Chartjs library
- [splot_dygraph.min.js](dist/splot_dygraph.min.js) - the Dygraph implementation of Splot, which includes the minified Dygraphs library

## Examples
- [Splot Chartjs Basic Example](https://lucidlylogicole.gitlab.io/splot/examples/chartjs_example.html)
- [Splot Chartjs Bar Chart Example](https://lucidlylogicole.gitlab.io/splot/examples/chartjs_bar_example.html)
- [Splot Dygraphs Basic Example](https://lucidlylogicole.gitlab.io/splot/examples/dygraph_example.html)

## API

**Note: Almost all options are optional and don't have to be specified**

- **Splot(options)** - main Plotting object
    - **options** _dict_
        - **parent**:`undefined`   - _parent element (id or element) to add chart to_
        - **type**:'`scatter`'     - _scatter, bar, line, pie_
        - **title**:`''`           - _chart title_
        - **xlabel**:`''`          - _x axis label_
        - **ylabel**:`''`          - _y axis label_
        - **xzero**:`false`        - _x axis start at 0_
        - **yzero**:`false`        - _y axis start at 0_
        - **legend**:`false`       - _display legend_
        - **labels**:`undefined`   - _array of labels required for non scatter type (bar, line) charts_
        - **grid**:`true`          - _display grid_
        - **dark**:`false`         - _dark colors mode_
        - **y2**:`false`           - _show 2nd y axis_
        - **y2zero**:`false`       - _y2 axis start at 0_
        - **y2label**:`''`         - _y2 axis label_
        - **chartjs**:`{}`         - _set other [Chartjs options](https://www.chartjs.org/docs/latest/)_
        - **dygraph**:`{}`         - _set other [Dygraphs options](https://dygraphs.com/options.html)_

- **Splot.addData(data_options)** - add a dataset to the chart by specifying x and y or a data array
    - **data_options** _dict_
        - **x**:`[1,2,...n]`           - _x data_
        - **y**:`[1,2,...n]`           - _y data_
        - **data**:`[[1,..n],[1,..n]]` - _specify data instead of using x and y above_
        - **label**: `'dataset'`       - _data label_
        - **line**:`1`                 - _line width (0 for no line)_
        - **points**:`3`               - _point size (0 for no points/markers)_
        - **color**:`'rgb(20,30,40)'`  - _valid html color_
        - **lineDash**:`[8,6]`         - _Specify a dashed line [length, space]_

- **Splot.clear()** - clear the chart
- **Splot.resetZoom()** - reset zoom to start
- **Splot.setTitles(title_options)** - set or update the title and axis labels
    - **title_options** _dict_
        - **title**:`''`           - _chart title_
        - **xlabel**:`''`          - _x axis label_
        - **ylabel**:`''`          - _y axis label_
- **Splot.update()** - update the plot
- **Splot.chart** - access the chartjs or dygraph object

--------------------------------------------------------------------------------
## Splot-Chartjs

### Usage

    <script src="dist/splot_chartjs.min.js"></script>
    <div id="chart_element"></div>
    <script>
        let chart = Splot({parent:'chart_element',title:'Chart'})
        chart.addData({x:[1,2,3],y:[1,4,9]})
    </script>

- [splot_chartjs.min.js](dist/splot_chartjs.min.js) includes both *chartjs* and the *chartjs zoom* plugin.


### Example - Chartjs

    let chart = Splot({
        parent:undefined,   // parent element (id or element)
        type:'scatter',     // scatter, bar, line, pie
        title:'',           // chart title
        xlabel:'',          // x axis label
        ylabel:'',          // y axis label
        xzero:false,        // x axis start at 0
        yzero:false,        // y axis start at 0
        legend:false,       // display legend
        labels:undefined,   // array of labels for non scatter type charts
        dark:false,         // dark colors mode
        
        y2:false,           // show 2nd y axis
        y2zero:false,       // y2 axis start at 0
        y2label:'',         // y2 axis label
        chartjs:{},         // override chartjs options
    })
    
    
    // Adding Data
    chart.addData({             // Add data
        x:[1,2,...n],           // x data
        y:[1,2,...n],           // y data
        label: 'dataset',       // data label
        line:1,                 // line width (0 for no line)
        points:3,               // point size (0 for no points/markers)
        color:'rgb(20,30,40)',  // valid html color
        lineDash:[8,6],         // Specify a dashed line [length, space] (opt)
    })    
    chart.removeData(index) // Remove dataset
    
    // Set/Change Titles after creation
    chart.setTitles({
        x:'x',
        title:'new title',
        y:'' // blank string removes, undefined doesn't change the title
    })
    
    // Other Options
    chart.clear()           // Clear the chart
    chart.resetZoom()       // reset the zoom
    chart.update()          // update the plot
    
    chart.chart             // access the Chartjs created object


----------------------------------------------------------
## Splot-Dygraph

The Dygraph chart is useful for displaying large scatter data sets, but does not have other chart types.

### Usage

    <script src="dist/splot_dygraph.min.js"></script>
    <div id="chart_element"></div>
    <script>
        let chart = Splot({parent:'chart_element',title:'Chart'})
        chart.addData({x:[1,2,3],y:[1,4,9]})
    </script>

- [splot_dygraph.min.js](dist/splot_dygraph.min.js) includes *dygraph*, so there are no other dependencies


### Example - Dygraph

    let chart = Splot({
        parent:undefined,   // parent element (id or element)
        title:'',           // chart title
        xlabel:'',          // x axis label
        ylabel:'',          // y axis label
        legend:false,       // display legend
        
        y2:false,           // show 2nd y axis
        y2zero:false,       // y2 axis start at 0
        y2label:'',         // y2 axis label
        dygraphjs:{},       // override dygraphs options
    })
    
    
    // Adding Data
    chart.addData({             // Add data
        x:[1,2,...n],           // x data
        y:[1,2,...n],           // y data
        label: 'dataset',       // data label
        line:1,                 // line width (0 for no line)
        points:3,               // point size (0 for no points/markers)
        color:'rgb(20,30,40)',  // valid html color
        lineDash:[8,6],         // Specify a dashed line [length, space] (opt)
    })    
    chart.removeData(index) // Remove dataset
    
    // Set/Change Titles after creation
    chart.setTitles({
        x:'x',
        title:'new title',
        y:'' // blank string removes, undefined doesn't change the title
    })
    
    // Other Options
    chart.clear()           // Clear the chart
    chart.resetZoom()       // reset the zoom
    chart.update()          // update the plot
    
    chart.chart             // access the Dygraphs created object